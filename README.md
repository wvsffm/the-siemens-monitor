# The Siemens Monitor

*  [Ziel](https://gitlab.com/wvsffm/the-siemens-monitor/issues/1)
*  [Kanban-Board](https://gitlab.com/wvsffm/the-siemens-monitor/-/boards)

## Vereinbarungen

*  Formuliere Systemfeatures als User-Story.
*  Verwende ein Issue als Story-Card für eine User-Story.
*  Formuliere Arbeitsschritte einer User-Story als Task.
*  Löse einen Task einer User Story.
*  Für Entscheidungen und Diskussion dieser Entscheidungen steht das Wiki zur Verfügung.

## Definition of Done

